const timeRegExp = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/

export function parseLyric(lyricStr){
  let lyricStrs = lyricStr.split("\n")
  let lyricInfos = []
  for (const lineStr of lyricStrs) {
    let RegExpLyric = timeRegExp.exec(lineStr)
    if(!RegExpLyric) continue
    // 获取时间
    let minute = RegExpLyric[1]*60*1000
    let second = RegExpLyric[2]*1000
    let millsecondTime = RegExpLyric[3]
    let millsecond = millsecondTime.length === 2?millsecondTime*10:millsecondTime*1
    let time = minute + second + millsecond
    // 获取歌词
    let text = lineStr.replace(RegExpLyric[0],"") 
    lyricInfos.push({time,text})
  }
  return lyricInfos
}