import {getRankings} from "../service/api_music"
import {HYEventStore} from "hy-event-store"

const rankingsMap = {0:"newRankings",1:"hotRankings",2:"orignRankings",3:"upRankings"}

const rankingStore = new HYEventStore({
  state:{
    hotRankings:{},
    newRankings:{},
    orignRankings:{},
    upRankings:{}
  },
  actions:{
    getRankingData(ctx){
      for (let i = 0; i < 4; i++) {
        getRankings(i).then(res=>{
          let temp = rankingsMap[i]
          ctx[temp] = res.playlist
        })          
      }
    }
  }
})

export {
  rankingStore,
  rankingsMap
}