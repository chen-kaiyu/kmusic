import {HYEventStore} from "hy-event-store"
import {getSongDetail,getLyric} from "../service/api_player"
import {parseLyric} from "../utils/parse-lyric"

// const audioContext = wx.createInnerAudioContext()
const audioContext =  wx.getBackgroundAudioManager()

const playerStore = new HYEventStore({
  state:{
    isFirstPlay:true,
    isStop:false,

    id:0,
    currentSong:{},
    durationTime:0,
    lyricInfos:[],

    currentTime:0,
    currentLyricText:"",
    currentLyricIndex:0,

    playModeIndex:0, // 0: 循环播放 1: 单曲循环 2: 随机播放
    playList:[],
    playListIndex:0,

    isPlay:true
  },
  actions:{
    // 开始播放
    playerData(ctx,{id,isRefresh = false}){
      if(ctx.id === id && !isRefresh){
        this.dispatch("changePlayStatusAction",true)
        return
      }
      // 歌曲数据初始化
      ctx.id = id
      ctx.currentSong={}
      ctx.durationTime=0  
      ctx.lyricInfos=[]
      ctx.currentTime=0
      ctx.currentLyricText=""
      ctx.currentLyricIndex=0

      // 请求歌曲详情
      getSongDetail(id).then(res=>{
        ctx.currentSong = res.songs[0]
        ctx.durationTime = res.songs[0].dt
        audioContext.title = res.songs[0].name
      })
      // 请求歌词
      getLyric(id).then(res=>{
        let lyric = res.lrc.lyric
        let lyricInfos = parseLyric(lyric)
        ctx.lyricInfos = lyricInfos
      })
      // 播放音乐
      audioContext.stop()
      audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`
      audioContext.title = id
      audioContext.autoplay = true
      ctx.isPlay = true
      // 监听audioContext事件
      if(ctx.isFirstPlay){
        this.dispatch("AudioContextListenerAction")
        ctx.isFirstPlay = false
      }
    },
    // 播放监听
    AudioContextListenerAction(ctx){
      audioContext.onCanplay(()=>{
        audioContext.play()
      })
       // 监听时间改变
      audioContext.onTimeUpdate(()=>{
        // 获取音频当前播放到的时间
        let currentTime = audioContext.currentTime * 1000
        ctx.currentTime = currentTime
        // 歌词索引
        let i = 0
        for (; i < ctx.lyricInfos.length; i++) {
          let lyricInfo = ctx.lyricInfos[i]
          if(currentTime<lyricInfo.time) break
        }
        let currentIndex = i - 1 
        if(i == 0) currentIndex =0
        if(ctx.currentLyricIndex != currentIndex){
          let currentLyricText = ctx.lyricInfos[currentIndex].text
          ctx.currentLyricText =currentLyricText
          ctx.currentLyricIndex = currentIndex
        }
      })
      audioContext.onEnded(()=>{
        this.dispatch("playListAction")
      })
      audioContext.onPlay(()=>{
        ctx.isPlay = true
      })
      audioContext.onPause(()=>{
        ctx.isPlay = false
      })
      audioContext.onStop(()=>{
        ctx.isPlay = false
        ctx.isStop = true
      })
    },
    // 播放暂停状态
    changePlayStatusAction(ctx,isPlay = true){
      ctx.isPlay = isPlay
      if(ctx.isPlay && ctx.isStop){
        audioContext.src = `https://music.163.com/song/media/outer/url?id=${ctx.id}.mp3`
        audioContext.title = ctx.currentSong.name
      }
      ctx.isPlay?audioContext.play():audioContext.pause()
    },
    // 前后歌曲跳转和歌曲列表
    playListAction(ctx,isNext = true){
      let index = ctx.playListIndex
      let list = ctx.playList
      switch (ctx.playModeIndex) {
        case 0:
          index=isNext?index+1:index-1
          if(index == -1) index = list.length-1
          if(index == list.length) index = 0
          break;
        case 1:
          break;
        case 2:
          index = Math.floor(Math.random()*list.length) 
          break;
      }
      let currentSong = list[index]
      if(!currentSong)currentSong = ctx.currentSong
      else ctx.playListIndex = index
      this.dispatch("playerData",{id:currentSong.id,isRefresh:true})
    }
  }
}) 

export{
  audioContext,playerStore
}