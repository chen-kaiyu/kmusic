// components/player-list/index.js
import {playerStore} from "../../store/index"
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    playList:{
      type:Array,
      value:[]
    }
  },
  attached(){
    playerStore.onStates(["playListIndex","playList"],({playListIndex,playList})=>{
      if(playList !== undefined) this.setData({playList})
      if(playListIndex !== undefined) this.setData({playListIndex})
    })
  },
  /**
   * 组件的初始数据
   */
  data: {
    playList:[],
    playListIndex:0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    playListSongs(event){
      let index = event.currentTarget.dataset.index
      playerStore.setState("playListIndex",index)
      playerStore.dispatch("playerData",{id:this.data.playList[index].id})
    }
  }
})
