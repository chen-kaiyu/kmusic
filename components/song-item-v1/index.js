// components/song-item-v1/index.js
import {playerStore} from "../../store/index"

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type:Object,
      value:{}
    }
  },
  methods: {
    songClick(){
      let id = this.properties.item.id
      wx.navigateTo({
        url: `/pages/music-player/index?id=${id}`,
      })
      playerStore.dispatch("playerData",{id})
    }
  }
})
