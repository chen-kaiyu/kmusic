// const BASE_URL = "http://123.207.32.32:9001"
const BASE_URL = "https://www.ckyhahaha.info"

class CRequest {
  request(url,method,params){
    return new Promise((resolve,reject)=>{
      wx.request({
        url: BASE_URL+url,
        method:method,
        data:params,
        success:function(res){
          resolve(res.data)
        },
        fail:reject
      })
    })
  }
  get(url,params){
    return this.request(url,"GET",params)
  }
  post(url,params){
    return this.request(url,"POST",params)
  }
}

const cRequest = new CRequest()

export default cRequest