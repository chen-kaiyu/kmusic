import cRequest from "./index"

export function getBanners(){
  return cRequest.get("/banner" , {
    type:2
  })
}

export function getRankings(idx){
  return cRequest.get("/top/list",{
    idx
  })
}

export function getSongMenu(cat = "全部",limit = 6,offset=0){
  return cRequest.get("/top/playlist",{
    cat,
    limit,
    offset
  })
}

export function getSongMenuDetail(id){
  return cRequest.get("/playlist/detail/dynamic",{
    id
  })
}
