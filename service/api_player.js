import cRequest from "./index"

export function getSongDetail(ids){
  return cRequest.get("/song/detail",{
    ids
  })
}

export function getLyric(id){
  return cRequest.get("/lyric",{
    id
  })
}

// export function getSongMedia(id){
//   return cRequest.get("/song/media/outer/url",{
//     id:id+".mp3"
//   })
// }