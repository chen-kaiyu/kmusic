import cRequest from "./index"

export function getSearchHot(){
  return cRequest.get("/search/hot")
}
export function getSearchSuggest(keywords){
  return cRequest.get("/search/suggest",{
    keywords,
    type:"mobile"
  })
}

export function getSearchResult(keywords){
  return cRequest.get("/search",{
    keywords
  })
}