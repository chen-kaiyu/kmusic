import cRequest from "./index"

//请求mv数据
export function getTopMV(offset,limit=10){
  return cRequest.get("/top/mv",{
    offset:offset,
    limit:limit
  })
}

//mv地址
export function getMVURL(id){
  return cRequest.get("/mv/url",{
    id
  })
}

//mv信息
export function getMVDetail(mvid){
  return cRequest.get("/mv/detail",{
    mvid
  })
}

//相关mv
export function getRelatedVideo(id){
  return cRequest.get("/related/allvideo",{
    id
  })
}

// video的播放
export function getVideoPlayer(id){
  return cRequest.get("/video/url",{
    id
  })
}