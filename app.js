// app.js
App({
  onLaunch() {
    const info = wx.getSystemInfoSync()
    this.globalData.screenWidth = info.screenWidth
    this.globalData.screenHeight = info.screenHeight
    this.globalData.statusBarHeight = info.statusBarHeight
    let deviceRadio = info.screenHeight/info.screenWidth
    this.globalData.deviceRadio = deviceRadio
  },
  globalData: {
    deviceRadio:0,
    screenWidth:0,
    screenHeight:0,
    statusBarHeight:0,
    navBarHeight:44,
  }
})
