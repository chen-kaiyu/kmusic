// pages/detail-video/index.js
import {getMVURL,getMVDetail,getRelatedVideo} from "../../service/api_video"

Page({
  data: {
    mvDetail:{},
    mvURL:{},
    relatedVideos:[]
  },
  onLoad: function (options) {
    const id = options.id
    //获取页面数据
    this.getPageData(id)
  },
  getPageData(id){
    getMVURL(id).then(res=>{
      this.setData({mvURL:res.data})
    })
    getMVDetail(id).then(res=>{
      this.setData({mvDetail:res.data})
    })
    getRelatedVideo(id).then(res=>{
      this.setData({relatedVideos:res.data})
    })
  }
})