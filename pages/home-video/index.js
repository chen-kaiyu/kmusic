import {getTopMV} from "../../service/api_video"

Page({
  data: {
    topMvs:[],
    hasMore:true
  },
  onLoad: function () {
    this.getTopMVData(0)
  },
  // 获取数据
  getTopMVData : async function(offset){
    //判断是否还有更多数据可以请求
    if(!this.data.hasMore){
      wx.stopPullDownRefresh()
      return
    }

    //展示加载图标
    wx.showNavigationBarLoading()

    //调用接口，保存数据
    const res = await getTopMV(offset)
    let newData = this.data.topMvs
    if(offset == 0){
      newData = res.data
    } else{
      newData = newData.concat(res.data)
    }
    this.setData({topMvs:newData})
    this.setData({hasMore:res.hasMore})
    wx.hideNavigationBarLoading()
    if(offset == 0){
      wx.stopPullDownRefresh()
    }
  },

  //进入详情页
  toVideoDetail(event){
    const id = event.currentTarget.dataset.item.id
    wx.navigateTo({
      url:`/pages/detail-video/index?id=${id}`
    })
  },

  //下拉刷新
  onPullDownRefresh(){
    this.getTopMVData(0)
  },

  //上拉加载
  onReachBottom(){
    this.getTopMVData(this.data.topMvs.length)
  }
})