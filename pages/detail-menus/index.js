import {getSongMenu} from "../../service/api_music"

const cats = {"all":"全部","fashion":"流行"}

Page({
  data: {
    menuData:[],
    cat:""
  },
  onLoad: function (options) {
    let cat = cats[options.cat]
    this.setData({cat})
    this.getPageData(cat)
  },
  // 数据请求
  getPageData:async function(cat,limit=10,offset=0){
    wx.showNavigationBarLoading()
    let res = await getSongMenu(cat,limit,offset)
    let menuData = this.data.menuData
    if(offset == 0) menuData = res.playlists
    else{
      menuData = menuData.concat(res.playlists)
    }
    this.setData({menuData})
    wx.hideNavigationBarLoading()
    if(offset == 0) wx.stopPullDownRefresh()
  },
  //下拉刷新
  onPullDownRefresh(){
    let cat = this.data.cat
    this.getPageData(cat)
  },
  //上拉加载
  onReachBottom(){
    let cat = this.data.cat
    this.getPageData(cat,10,this.data.menuData.length)
  },
  menuBtn(event){
    let item = event.currentTarget.dataset.item
      wx.navigateTo({
        url: `/pages/detail-songs/index?id=${item.id}&type=menu`,
      })
  }
})