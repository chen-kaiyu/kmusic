// pages/music-player/index.js
import {audioContext,playerStore} from "../../store/index"

const playModeName = ["order","repeat","random"]

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0,
    currentSong:{},
    durationTime:0,
    lyricInfos:[],

    currentTime:0,
    currentLyricText:"",
    currentLyricIndex:0,
    
    playModeIndex:0,
    playMode:"order",
    playListIndex:0,

    isPlay:true,
    playIcon:"pause", 

    isMusicLyric:true,
    isSliderChanging:false,
    currentPage:0,
    contentHeight:0,
    currentLyricScroll:0,
    sliderValue:0,
    showList:false
  },
  onLoad: function (options) {
    let id =options.id
    this.setData({id})
    // 初始化页面数据
    this.playerStoreListener(id)

    // 动态计算内容高度
    const globalData = getApp().globalData
    let screenHeight = globalData.screenHeight
    let statusBarHeight = globalData.statusBarHeight
    let navBarHeight = globalData.navBarHeight
    let deviceRadio = globalData.deviceRadio
    let contentHeight = screenHeight - statusBarHeight - navBarHeight
    this.setData({contentHeight , isMusicLyric:deviceRadio >= 2})
  },
  // 事件处理
  swiperChange(event){
    let current = event.detail.current
    this.setData({currentPage:current})
  },
  sliderChange(event){
    let sliderValue = event.detail.value 
    // 计算需要播放的currentTime
    let currentTime = this.data.durationTime*sliderValue/100
    // 音频播放到指定时间
    // audioContext.pause()
    audioContext.seek(currentTime/1000)
    this.setData({sliderValue,isSliderChanging:false})
  },
  sliderChanging(event){
    let sliderValue = event.detail.value
    let currentTime = this.data.durationTime*sliderValue/100
    this.setData({currentTime,isSliderChanging:true})
  },
  playModeBtn(){
    let temp = this.data.playModeIndex + 1
    if(temp === 3)temp = 0
    playerStore.setState("playModeIndex",temp)
  },
  playhandle(){
      playerStore.dispatch("changePlayStatusAction",!(this.data.isPlay))
  },
  nextSong(){
    playerStore.dispatch("playListAction")
  },
  prevSong(){
    playerStore.dispatch("playListAction",false)
  },
  showListBtn(){
    this.setData({showList:true})
  },
  closePlayListBtn(){
    this.setData({showList:false})
  },

  // store监听
  playerStoreListener(){
    playerStore.onStates(["currentSong","durationTime","lyricInfos"],({
      currentSong,durationTime,lyricInfos
    })=>{
      if(currentSong) this.setData({currentSong})
      if(durationTime) this.setData({durationTime})
      if(lyricInfos) this.setData({lyricInfos})
    })
    playerStore.onStates(["currentTime","currentLyricText","currentLyricIndex"],({
      currentTime,currentLyricText,currentLyricIndex
    })=>{
      if(currentTime && !this.data.isSliderChanging){
        let sliderValue = currentTime/this.data.durationTime*100
        this.setData({currentTime,sliderValue})
      }
      if(currentLyricIndex){
        this.setData({currentLyricScroll:currentLyricIndex*35,currentLyricIndex})
      }
      if(currentLyricText){
        this.setData({currentLyricText})
      }
    })
    playerStore.onStates(["playModeIndex","isPlay"],({playModeIndex,isPlay})=>{
      if(playModeIndex !== undefined){
        this.setData({playModeIndex,playMode:playModeName[playModeIndex]}) 
      }
      if(isPlay !== undefined){
        this.setData({
          isPlay,
          playIcon:isPlay?'pause':'resume'
        })
      }
    })
  }
})