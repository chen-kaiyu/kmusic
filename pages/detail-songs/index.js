import {rankingStore}from "../../store/index"
import {getSongMenuDetail} from "../../service/api_music"
import {playerStore} from "../../store/index"

// pages/detail-songs/index.js
Page({
  data: {
    type:"",
    ranking:"",
    songsInfo:{}
  },
  onLoad: function (options) {
    let type = options.type
    this.setData({type})
    if(type == "menu"){
      let id = options.id
      getSongMenuDetail(id).then(res=>{
        this.setData({songsInfo:res.playlist})
      })
    }else if(type == "rank"){
      let ranking = options.rankings
      this.setData({ranking})
      rankingStore.onState(ranking,this.getRankingData)
    }
  },
  onUnload:function(){
    if(this.data.ranking){
      rankingStore.offState(this.data.ranking,this.getRankingData)
    }
  },
  getRankingData(res){
    this.setData({songsInfo:res})
  },
  songBtn(event){
    let index = event.currentTarget.dataset.index
    playerStore.setState("playList",this.data.songsInfo.tracks)
    playerStore.setState("playListIndex",index)
  }
})