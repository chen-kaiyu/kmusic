// pages/home-music/index.js
import {getBanners,getSongMenu} from "../../service/api_music"
import queryRect from "../../utils/query-rect"
import throttle from "../../utils/throttle"
import {rankingStore,rankingsMap,playerStore} from "../../store/index"

const throttleQueryRect = throttle(queryRect,1000,{trailing:true})

Page({
  data: {
    swiperHeight:0,
    banners:{},
    recommendSongs:[],
    hotSongMenu:[],
    recommendSongsMenu:[],
    rankings:{0:{},1:{},2:{},3:{}},

    currentSong:{},
    isPlay:true,
    playAnimState:"",
    playList:[],
    playListIndex:0,
    showList:false
  },
  onLoad: function (options) {
    this.getPageData()
    //共享数据请求
    rankingStore.dispatch("getRankingData")
    
    this.setupplayerStoreListener()
  },

  //网络请求
  getPageData(){
    getBanners().then(res=>{
      let banners = []
      banners = res.banners.filter(element=>{
        return element.song
      })
      this.setData({banners})
    }),
    getSongMenu().then(res=>{
      this.setData({hotSongMenu:res.playlists})
    }),
    getSongMenu("流行").then(res=>{
      this.setData({recommendSongsMenu:res.playlists})
    })
  },
  //搜索页面跳转
  searchClick(){
    wx.navigateTo({
      url: '/pages/detail-search/index',
    })
  },

  //事件处理
  swiperImgLoad(){
    throttleQueryRect(".swiper-image").then(res=>{
      this.setData({swiperHeight:res[0].height})
    })
  },
  handelHomeSwiper(event){
    let swiperIndex = event.currentTarget.dataset.swiperindex
    if(this.data.banners[swiperIndex].song){
      let id = this.data.banners[swiperIndex].song.id
      wx.navigateTo({
        url: `/pages/music-player/index?id=${id}`,
      })
      playerStore.dispatch("playerData",{id})
      let bannerSongs = []
      this.data.banners.forEach(item=>{
        bannerSongs.push(item.song)
      })
      playerStore.setState("playList",bannerSongs)
      playerStore.setState("playListIndex",swiperIndex)
    }
  },
  songBtn(event){
    let index = event.currentTarget.dataset.index
    playerStore.setState("playList",this.data.recommendSongs)
    playerStore.setState("playListIndex",index)
  },
  playbtn(){
    playerStore.dispatch("changePlayStatusAction",!(this.data.isPlay))
  },
  playBarBtn(){
    wx.navigateTo({
      url:`/pages/music-player/index?id=${this.data.currentSong.id}`
    })
  },
  showListBtn(){
    this.setData({showList:true})
  },
  closePlayListBtn(){
    this.setData({showList:false})
  },

  // 更多歌单
  moreHotMenu(){
    this.navigateToDetailMenu("all")
  },
  moreRecommendMenu(){
    this.navigateToDetailMenu("fashion")
  },
  navigateToDetailMenu(cat){
    wx.navigateTo({
      url:`/pages/detail-menus/index?cat=${cat}`
    })
  },

  //巅峰版数据重构
  rankingHander(idx){
    return  (res)=>{
      if(Object.keys(res).length === 0) return
      let name = res.name;
      let pic = res.coverImgUrl;
      let playCount = res.playCount;
      let songLists = res.tracks.slice(0,3);
      let temp = {name,pic,playCount,songLists}
      const newRankings = {...this.data.rankings,[idx]:temp}
      this.setData({rankings:newRankings})
    }
  },

  // 跳转歌曲详情页
  rightClick(){
    this.navigateToSongsDetail("hotRankings")
  },
  rankingClick(event){
    let idx = event.target.dataset.idx
    let temp = rankingsMap[idx]
    this.navigateToSongsDetail(temp)
  },
  navigateToSongsDetail(rankingName){
    wx.navigateTo({
      url: `/pages/detail-songs/index?rankings=${rankingName}&type=rank`,
    })
  },

  // store数据监听
  setupplayerStoreListener(){
    //排行榜监听
    rankingStore.onState("hotRankings" , res=>{
      if(!res.tracks)return 
      const recommendSongs = res.tracks.slice(0,6)
      this.setData({recommendSongs})
    })
    rankingStore.onState("newRankings",this.rankingHander(0))
    rankingStore.onState("hotRankings",this.rankingHander(1))
    rankingStore.onState("orignRankings",this.rankingHander(2))
    rankingStore.onState("upRankings",this.rankingHander(3))
    // 播放器监听
    playerStore.onStates(["currentSong","isPlay","playList", "playListIndex"],
    ({currentSong,isPlay,playList, playListIndex})=>{
      if(currentSong) this.setData({currentSong})
      if(isPlay !== undefined){
        this.setData({isPlay,playAnimState:isPlay?'running':'paused'})
      } 
      if(playList) this.setData({playList})
      if(playListIndex) this.setData({playListIndex})
    })
  }
})